#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 2016
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
import unittest
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)
        
    def test_read_2(self):
        s = "10 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 10)
        self.assertEqual(j, 10)
        
    # Expected failure
    @unittest.expectedFailure
    def test_read_3(self):
        s = "5024 1098\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 1098)
        
    @unittest.expectedFailure
    def test_read_4(self):
        s = "5024 1098\n"
        i, j = collatz_read(s)
        self.assertEqual(j, 5024)

    # ----
    # eval
    # ----
    
    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(200, 100)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)
        
    def test_eval_5(self):
        v = collatz_eval(10, 10)
        self.assertEqual(v, 7)
        
    # Edge Case
    def test_eval_6(self):
        v = collatz_eval(1, 999999)
        self.assertEqual(v, 525)
        
    def test_eval_7(self):
        v = collatz_eval(999998, 999999)
        self.assertEqual(v, 259)
        
    def test_eval_8(self):
        v = collatz_eval(1, 2)
        self.assertEqual(v, 2)
        
    # Expected failure
    @unittest.expectedFailure
    def test_eval_9(self):
        v = collatz_eval(238, 490)
        self.assertEqual(v, 142) # Should be 144
    
    @unittest.expectedFailure
    def test_eval_10(self):
        v = collatz_eval(7230, 10000)
        self.assertEqual(v, 261) # Should be 260
    
    @unittest.expectedFailure
    def test_eval_11(self):
        v = collatz_eval(7230, 10000)
        self.assertEqual(v, 259) # Should be 260
    
    @unittest.expectedFailure
    def test_eval_12(self):
        v = collatz_eval(9, 10)
        self.assertEqual(v, 7) # Should be 20
    
    
    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")
        
    def test_print_2(self):
        w = StringIO()
        collatz_print(w, 999998, 999999, 259)
        self.assertEqual(w.getvalue(), "999998 999999 259\n")
        
    # Expected failure
    @unittest.expectedFailure
    def test_print_3(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "10 1 20\n")
        
    @unittest.expectedFailure
    def test_print_4(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "20 10 1\n")
        
    @unittest.expectedFailure
    def test_print_5(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 20 10\n")
        
    @unittest.expectedFailure
    def test_print_6(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 20 20\n")
        
    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")
            
    # Edge Case
    def test_solve_2(self):
        r = StringIO("1 999999\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 999999 525\n100 200 125\n201 210 89\n900 1000 174\n")
            
    def test_solve_3(self):
        r = StringIO("1 10\n100 200\n999998 999999\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n999998 999999 259\n900 1000 174\n")
            
    def test_solve_4(self):
        r = StringIO("1 10\n100 200\n201 210\n1 2\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n1 2 2\n")
            
    # Expected failure
    @unittest.expectedFailure
    def test_solve_5(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n 100 200 125\n 201 210 89\n 900 1000 174\n ")
# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
% coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


% cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


% coverage report -m                   >> TestCollatz.out



% cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
